# Import libraries:
import pandas as pd 
import psycopg2 as psy
import getpass
import random as rd

# Create a new database : 
username = input('Username: ') # set your username 
data = pd.read_csv(r'visualisation/dataPropre.csv') # set the path to your csv file
df = pd.DataFrame(data) # create a dataframe from the csv file
df = df.drop_duplicates() # drop duplicates
df = df.dropna() # drop NaN values

co = None # set the connection to None

# create a new database connection with the username
try:
    co = psy.connect(host='londres', # set the host
                    database="db"+username, # set the database name
                    user=username, # set the username
                    password = getpass.getpass('Mot de passe : '))  # set the password

    curs = co.cursor() # create a cursor
    # drop tables if they already exists :
    curs.execute ('''DROP TABLE IF EXISTS Compose CASCADE;''') 
    curs.execute ('''DROP TABLE IF EXISTS Track CASCADE;''') 
    curs.execute ('''DROP TABLE IF EXISTS Produce CASCADE;''')
    curs.execute ('''DROP TABLE IF EXISTS Artist CASCADE;''')
    curs.execute ('''DROP TABLE IF EXISTS Album CASCADE;''')
    curs.execute ('''DROP TABLE IF EXISTS Genre CASCADE;''')
    
    # create tables :
    curs.execute ('''CREATE TABLE Genre (
        genre_id varchar (170),
        genre varchar (30000),
        PRIMARY KEY(genre_id)
        );''')
    curs.execute ('''CREATE TABLE Album (
        album_id varchar (170),
        album_name varchar (3000),
        PRIMARY KEY(album_id)
        );''')
    curs.execute ('''CREATE TABLE Artist (
        artist_id varchar (170),
        artist_name varchar (3000),
        PRIMARY KEY(artist_id)
        );''')
    curs.execute ('''CREATE TABLE Produce (
        album_id varchar (170),
        artist_id varchar (170),
        PRIMARY KEY(artist_id, album_id),
        FOREIGN KEY (artist_id) REFERENCES Artist(artist_id),
        FOREIGN KEY (album_id) REFERENCES Album(album_id)
        );''')
    curs.execute ('''CREATE TABLE Track (
        track_id varchar (170),
        track_name varchar (3000),
        popularity numeric (4),
        duration_ms numeric (8),
        explicit varchar(5),
        danceability numeric,
        energy numeric,
        loudness numeric,
        speechiness numeric,
        acousticness numeric,
        instrumentalness numeric,
        liveness numeric,
        valence numeric,
        tempo numeric,
        PRIMARY KEY(track_id),
        genre_id varchar (170),
        album_id varchar (3000),
        FOREIGN KEY (genre_id) REFERENCES Genre(genre_id),
        FOREIGN KEY (album_id) REFERENCES Album(album_id)
        );''')
    
    curs.execute ('''CREATE TABLE Compose (
        artist_id varchar (170),
        track_id varchar (3000),
        PRIMARY KEY(artist_id, track_id),
        FOREIGN KEY (artist_id) REFERENCES Artist(artist_id),
        FOREIGN KEY (track_id) REFERENCES Track(track_id)
        );''')
    co.commit()

    # Insert data into dictionaries:
    artists = {}
    albums = {}
    genres = {}

    # Insert genres into Genre table:
    i = 0
    for row in df.itertuples():
        genre = str(row.track_genre) 
        if genre not in genres:
            i += 1
            query = f"INSERT INTO Genre VALUES ({i}, '{genre}')"
            curs.execute(query)
            genres[genre] = i
    co.commit()

    # Insert albums and artists into Album, Artist, and Produce tables :
    i = 0
    j = 0
    for row in df.itertuples():
        track_id = row.track_id
        artist_names = str(row.artists).split(";")
        album_name = row.album_name.strip().replace("'", "''")

        if album_name not in albums:
            j += 1
            query = f"INSERT INTO Album VALUES ({j}, '{album_name}')"
            curs.execute(query)
            albums[album_name] = j

        for artist_name in artist_names:
            artist_name = artist_name.strip().replace("'", "''")

            if artist_name not in artists:
                i += 1
                query = f"INSERT INTO Artist VALUES ({i}, '{artist_name}')"
                curs.execute(query)
                artists[artist_name] = i

            artist_id = artists[artist_name]
            album_id = albums[album_name]
            query = f"SELECT * FROM Produce WHERE artist_id = '{artist_id}' AND album_id = '{album_id}'"
            curs.execute(query)
            result = curs.fetchall()

            if len(result) == 0:
                query = f"INSERT INTO Produce (artist_id, album_id) VALUES ('{artist_id}', '{album_id}')"
                curs.execute(query)

    co.commit()

    # Insert data into Track table :
    for row in df.itertuples ():
        genre_id = genres[str(row.track_genre)]
        album_id = albums[row.album_name.strip().replace("'", "''")]
        curs.execute ('''INSERT INTO Track VALUES (%s ,%s ,%s ,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);''',
            (row.track_id , row.track_name , row.popularity, row.duration_ms, row.explicit, row.danceability, row.energy, row.loudness, row.speechiness, row.acousticness, row.instrumentalness, row.liveness, row.valence, row.tempo, genre_id, album_id))

    co.commit()

    # Insert into Compose table after populating Track table :
    for row in df.itertuples():
        track_id = row.track_id
        artist_names = str(row.artists).split(";")

        for artist_name in artist_names:
            artist_name = artist_name.strip().replace("'", "''")
            artist_id = artists[artist_name]
            query = f"INSERT INTO Compose (artist_id, track_id) VALUES ('{artist_id}', '{track_id}')"
            curs.execute(query)

    co.commit()


except (Exception, psy.DatabaseError) as error: # If there is an error
    print(error) # Print the error

finally:
    if co is not None: # If the connection is not closed
        co.close() # Close the connection to the database

print("Done !")
