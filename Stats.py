import pandas as pd
import psycopg2 as psy
import matplotlib . pyplot as plt
import getpass

username = input('Username: ')

connec = None

try:
    connec = psy.connect(host='londres', # Changer les variables host, database, user et password par vos propres identifiants afin de générer les lignes dans les tables.
                    database="db"+username,
                    user=username,
                    password = getpass.getpass('Mot de passe : ')) 
    

    #album avec le plus de chanson
    datafr = pd.read_sql ('''   
        SELECT album_name, COUNT(track_id) AS nb_tracks
        FROM Album
        JOIN Track USING (album_id)
        GROUP BY album_name
        ORDER BY nb_tracks DESC
        LIMIT 5;
        ''', con=connec )
    
    fig = plt.figure(figsize =(10, 7))
    plt.bar(datafr['album_name'], datafr['nb_tracks'])
    plt.title('Number of tracks of the 5 most prolific albums')
    plt.xlabel('Album')
    plt.ylabel('Number of tracks')
    plt.show()

    # Genre le plus present dans l'album avec le plus de chanson
    datafr = pd.read_sql ('''
    
                SELECT Album.album_name, Genre.genre, COUNT(Track.track_id) AS song_count
                FROM Album
                LEFT JOIN Track ON Album.album_id = Track.album_id
                LEFT JOIN Genre ON Track.genre_id = Genre.genre_id
                GROUP BY Album.album_name, Genre.genre
                ORDER BY song_count DESC
                LIMIT 5;
            ''', con=connec )
    
    fig = plt.figure(figsize =(10, 7))
    plt.bar(datafr['genre'], datafr['song_count'])

    plt.title('Genre of the 5 most prolific albums')
    plt.xlabel('Genre')
    plt.ylabel('Number of tracks')
    plt.show()

    # Quels sont le 5 genres les plus populaires en diagramme pie ?
    datafr = pd.read_sql ('''
        SELECT genre, AVG(popularity) AS avg_popularity
        FROM Track
        JOIN Genre ON Track.genre_id = Genre.genre_id
        GROUP BY genre
        ORDER BY avg_popularity DESC
        LIMIT 5;
    ''', con=connec )

    fig = datafr .plot.pie(y='avg_popularity', labels=datafr['genre'], autopct='%1.1f%%', figsize=(5, 5)) # Generation du graphique
    plt.title('Average popularity of songs by genre')
    plt.show()
    
    # Quelle est la distribution de la popularité des chansons par artiste pour les 10 artistes ayant le plus d'albums ?
    
    datafr = pd.read_sql ('''
        SELECT Artist.artist_name, AVG(Track.popularity) AS average_popularity
        FROM Artist
        JOIN (
            SELECT Produce.artist_id, Track.popularity
            FROM Produce
            JOIN Album ON Produce.album_id = Album.album_id
            JOIN Track ON Track.album_id = Album.album_id
            WHERE Produce.artist_id IN (
                SELECT artist_id
                FROM Produce
                GROUP BY artist_id
                ORDER BY COUNT(DISTINCT album_id) DESC
                LIMIT 10
            )
        ) AS subquery ON Artist.artist_id = subquery.artist_id
        JOIN Track ON Track.popularity = subquery.popularity
        GROUP BY Artist.artist_name;
    ''', con=connec )
    
    fig = datafr .plot.pie(y='average_popularity', labels=datafr['artist_name'], autopct='%1.1f%%', figsize=(5, 5)) # Generation du graphique
    plt.title('Average popularity of songs by artist')
    plt.show()

    # Quelle est la durée moyenne des chansons par genre ? (Requête utilisant AVG, GROUP BY → Diagramme à barres)
    datafr = pd.read_sql ('''
        SELECT Genre.genre, AVG(Track.duration_ms) AS average_duration
        FROM Track
        JOIN Genre ON Track.genre_id = Genre.genre_id
        GROUP BY Genre.genre
        ORDER BY average_duration DESC
        LIMIT 5;
    ''', con=connec )

    fig = plt.figure(figsize =(10, 7))
    plt.bar(datafr['genre'], datafr['average_duration'])
    plt.title('Average duration of songs by genre')
    plt.xlabel('Genre')
    plt.ylabel('Average duration (ms)')
    plt.show()

    # Quels sont les 10 artistes avec le plus de chansons explicites ?
    datafr = pd.read_sql ('''
        SELECT Artist.artist_name, COUNT(Track.track_id) AS explicit_song_count
        FROM Artist
        JOIN Compose ON Artist.artist_id = Compose.artist_id
        JOIN Track ON Compose.track_id = Track.track_id
        WHERE Track.explicit = 'true'
        GROUP BY Artist.artist_name
        ORDER BY explicit_song_count DESC
        LIMIT 10;
    ''', con=connec )

    fig = plt.figure(figsize =(10, 7))
    plt.bar(datafr['artist_name'], datafr['explicit_song_count'])
    plt.title('Number of explicit songs by artist')
    plt.xlabel('Artists')
    plt.ylabel('Number of explicit songs')
    plt.show()

    # Quelle est la popularité moyenne des chansons explicites versus non-explicites en pie avec legende ?
    datafr = pd.read_sql ('''
        SELECT Track.explicit, AVG(Track.popularity) AS average_popularity
        FROM Track
        GROUP BY Track.explicit;
    ''', con=connec )

    fig = datafr .plot.pie(y='average_popularity', labels=datafr['explicit'], autopct='%1.1f%%', figsize=(5, 5)) # Generation du graphique
    plt.title('Average popularity of explicit versus non-explicit songs')
    plt.show()
    
    # Quelle est la répartition de la liveness par genre pour les 5 genres les plus populaires ? 
    datafr = pd.read_sql ('''
        SELECT Genre.genre, AVG(Track.liveness) AS average_liveness
        FROM Genre
        JOIN Track ON Genre.genre_id = Track.genre_id
        WHERE Genre.genre_id IN (
            SELECT genre_id
            FROM Track
            GROUP BY genre_id
            ORDER BY AVG(popularity) DESC
            LIMIT 5
        )
        GROUP BY Genre.genre
        ORDER BY average_liveness DESC;
    ''', con=connec )

    fig = plt.figure(figsize =(10, 7))
    plt.bar(datafr['genre'], datafr['average_liveness'])
    plt.title('Average liveness by genre')
    plt.xlabel('Genre')
    plt.ylabel('Average liveness')
    plt.show()

    #Quels sont les 5 artistes avec le plus d'albums ? (Requête utilisant COUNT, GROUP BY, ORDER BY, LIMIT → Diagramme à barres)
    datafr = pd.read_sql ('''
        SELECT Artist.artist_name, COUNT(DISTINCT Produce.album_id) AS album_count
        FROM Artist
        JOIN Produce ON Artist.artist_id = Produce.artist_id
        GROUP BY Artist.artist_name
        ORDER BY album_count DESC
        LIMIT 5;
    ''', con=connec )

    fig = plt.figure(figsize =(10, 7))
    plt.bar(datafr['artist_name'], datafr['album_count'])
    plt.title('Number of albums by artist')
    plt.xlabel('Artists')
    plt.ylabel('Number of albums')
    plt.show()

    #Quels sont les 5 genres avec le plus grand nombre de chansons instrumentales (instrumentalness > 0.5 par exemple) ?
    datafr = pd.read_sql('''
        SELECT Genre.genre, COUNT(Track.track_id) AS instrumental_song_count
        FROM Genre
        JOIN Track ON Genre.genre_id = Track.genre_id
        WHERE Track.instrumentalness > 0.5
        GROUP BY Genre.genre
        ORDER BY instrumental_song_count DESC
        LIMIT 5;
    ''', con=connec )

    fig = plt.figure(figsize =(10, 7))
    plt.bar(datafr['genre'], datafr['instrumental_song_count'])
    plt.title('Number of instrumental songs by genre')
    plt.xlabel('Genre')
    plt.ylabel('Number of instrumental songs')
    plt.show()

except (Exception , psy.DatabaseError ) as error :
    print(error)
finally :
    if connec is not None:
        connec.close()
        print(' Database connection closed . ')
