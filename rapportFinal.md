# Compte Rendu de SAE 2.04
## Mathéo Hersan, Yvan Calatayud, Léo Tuaillon (Groupe 10)

![Logo](logo.png)



## **1. Présentation du jeu de données**

Notre jeu de données provient de Spotify et contient des informations sur des morceaux de musique appartenant à 113 genres différents. Chaque morceau est accompagné de certaines caractéristiques audio. Les données sont au format CSV, ce qui les rend faciles à charger et à manipuler.

Le jeu de données peut être utilisé pour diverses applications, telles que la construction d'un système de recommandation basé sur les préférences des utilisateurs, la classification des morceaux en fonction des caractéristiques audio et des genres disponibles, ou toute autre application que l'on peut imaginer.

### Les colonnes du jeu de données sont les suivantes :

- track_id : identifiant Spotify du morceau
- artists : noms des artistes ayant participé au morceau, séparés par un point-virgule s'il y en a plusieurs
- album_name : nom de l'album dans lequel apparaît le morceau
- track_name : nom du morceau
- popularity : popularité du morceau, variant de 0 à 100, calculée en fonction du nombre de lectures et de leur récence
- duration_ms : durée du morceau en millisecondes
- explicit : indique si le morceau contient des paroles explicites (true) ou non (false)
- danceability, energy, key, loudness, mode, speechiness, acousticness, instrumentalness, liveness, valence, tempo, time_signature : différentes caractéristiques audio du morceau
- track_genre : genre auquel appartient le morceau

Nous avons présenté ici les principales colonnes du jeu de données, mais il existe d'autres caractéristiques audio qui n'ont pas été détaillées. Ces caractéristiques supplémentaires peuvent également être exploitées pour approfondir l'analyse des morceaux de musique.

Lien vers le jeu de données : [[https://www.kaggle.com/datasets/maharshipandya/-spotify-tracks-dataset](https://www.kaggle.com/datasets/maharshipandya/-spotify-tracks-dataset)]

Dans le cadre de notre travail, nous avons effectué un pré-traitement des données pour éliminer les doublons, nettoyer les valeurs aberrantes et standardiser les formats. 

Nous avons décidé de supprimer trois colonnes : “key”, “mode”, “time_signature”. Nous avons fait ce choix car elles n’étaient pas pertinentes selon ce que nous voulions faire.


### Contexte

Dans une ère où la musique est omniprésente et accessible en quelques clics, il est crucial pour les artistes, les producteurs et les plateformes de streaming de comprendre les tendances et les préférences musicales du public. C'est dans ce contexte que nous nous sommes intéressés au dataset de Spotify. Ce dernier offre un large éventail de genres musicaux et une riche variété d'attributs pour chaque morceau, offrant une opportunité unique de comprendre la popularité musicale à travers différents genres et périodes.

Notre objectif est d'analyser et de comparer la popularité des différents genres musicaux, en mettant en avant les genres les plus populaires et en observant leur évolution dans le temps. En raison de la grande diversité des genres (113 au total), nous concentrerons notre analyse sur certains genres spécifiques et sur les genres les plus populaires.


## **2.Modèles de données**
### MCD :
![MCD](MCD-MLD/MCD.svg)
### MLD :
![MLD](MCD-MLD/MLD.svg)

Dans notre base de données, nous avons choisi de concevoir un MLD qui repose sur six tables principales : Genre, Album, Artist, Produce, Track, et Compose.

- **Table Genre** : Cette table sert à catégoriser les sons en différents genres musicaux. Chaque genre est unique et est identifié par un genre_id. Cette table est nécessaire car elle permet une analyse par genre, un aspect crucial dans l'industrie musicale.

- **Table Album** : Cette table contient les albums musicaux. Chaque album est unique et est identifié par un album_id. Cette table permet de regrouper les sons qui font partie du même album, ce qui est très courant sur les plateformes de streaming comme Spotify.

- **Table Artist** : Cette table contient les noms des différents artistes. Chaque artiste est unique et est identifié par un artist_id. Cette table est nécessaire car elle repertories tous les différents artistes de notre jeu de données.

- **Table Produce** : Cette table sert de table de jointure entre les tables Artist et Album. Elle permet de représenter la relation de production entre un artiste et un album. Chaque paire d'artist_id et d'album_id est unique, ce qui permet le fait qu'un artiste peut produire plusieurs albums et qu'un album peut être produit par plusieurs artistes.

- **Table Track** : Cette table contient des informations détaillées sur les sons individuelles, y compris leur nom, leur popularité, leur durée, leur explicité, ainsi que plusieurs caractéristiques musicales comme la dansabilité, l'énergie, la sonorité... Chaque son est unique et est identifiée par un track_id. Cette table est liée aux tables Genre et Album par des clés étrangères, ce qui permet d'associer chaque son à un genre et à son album.

- **Table Compose** : Cette table sert de table de jointure entre les tables Artist et Track. Elle permet de représenter la relation de composition entre un artiste et un son. Chaque paire d'artist_id et de track_id est unique, ce qui permet le fait qu'un artiste peut composer plusieurs sons et qu'un son peut être composée par plusieurs artistes.

En conclusion, la conception de notre MLD vise à refléter fidèlement les complexités et les nuances de l'industrie musicale. Elle nous permet de mener une analyse approfondie à plusieurs niveaux, y compris le niveau de l'artiste, de l'album, de la sons et du genre.

## **3. Questions et visualisation des données**
### 1) Quels sont les albums avec le plus de morceaux ? Et quels sont les genres le plus présents dans ces albums ?

<img src="visualisation-img/1.png" height="400" align="center">


- Les cinq artistes les plus prolifiques de Spotify se distinguent par des différences marquées dans le nombre de morceaux présents dans leurs albums respectifs. Les chiffres varient de 110 morceaux pour "The Complete Grant Williams" à 68 morceaux pour "Hans Zimmer: Epic Scores". Certains artistes ont une discographie vaste, offrant aux auditeurs une abondance de morceaux à découvrir, tandis que d'autres privilégient des compilations plus succinctes. 

<img src="visualisation-img/2.png" height="400" align="center">


- Par ailleurs, il est intéressant de constater la présence de genres musicaux spécifiques parmi ces albums. Le genre "honky-tonk" est représenté, témoignant de la diversité musicale des artistes étudiés. De plus, on observe une inclination vers la musique classique, ainsi que des influences allemandes. Ces conclusions soulignent la variété musicale des artistes les plus prolifiques de Spotify.

### 3) Quels sont le 5 genres les plus populaires ?
<img src="visualisation-img/3.png" height="450" align="center">

Le diagramme circulaire révèle les cinq genres musicaux les plus populaires sur Spotify : la K-pop, le pop-film, le metal, le chill et le latino. Ces résultats indiquent une préférence marquée des auditeurs pour ces genres spécifiques, reflétant leur popularité moyenne élevée. La K-pop et le pop-film témoignent de l'attrait mondial pour la musique coréenne et les chansons de films, tandis que le metal conserve toujours une base de fans solide. Le genre chill indique une demande pour une musique relaxante, tandis que le latino reflète l'influence et la popularité de la musique latine à l'échelle internationale. Ces résultats soulignent les tendances actuelles des auditeurs en matière de musique.

### 4) Quelle est la distribution de la popularité des chansons par artiste pour les 10 artistes ayant le plus d'albums ?
<img src="visualisation-img/4.png" height="450" align="center">

Le diagramme représente la popularité moyenne des chansons des 10 artistes avec le plus d'albums. Shreya Ghoshal, moins connue mondialement, possède la plus grande part de popularité (19,8%), indiquant une écoute régulière de ses titres, et donc un potentiel revenu élevé pour Spotify. À l'inverse, Justin Bieber, une icône mondiale, ne détient que 0,1% de la popularité moyenne. Cette constatation souligne que la notoriété d'un artiste ne garantit pas une popularité élevée pour l'ensemble de ses chansons, ni un revenu élevé pour Spotify. En bref, le graphique montre que la popularité moyenne d'un artiste, influencée par le volume de sa discographie, ne reflète pas nécessairement sa renommée ni sa rentabilité pour une plateforme de streaming.

### 5)  Quelle est la durée moyenne des chansons par genre ? 
<img src="visualisation-img/5.png" height="400" align="center">

Le graphique représente la durée moyenne des chansons pour les genres "Detroit-Techno", "Minimal Techno", "Chicago House", "Iranian" et "Techno". Ces genres, majoritairement liés à la musique électronique, tendent à proposer des morceaux plus longs, variant de 320 000 ms à 350 000 ms (environ de 5:20' à 5:50' ).

Cela suggère une préférence pour des sons plus longs dans ces genres, influençant potentiellement les recommandations de playlists sur des plateformes comme Spotify. Par exemple, ces genres pourraient être proposés à des auditeurs favorisant des sessions d'écoute prolongées.

### 6) Quels sont les 10 artistes avec le plus de chansons explicites ?
<img src="visualisation-img/6.png" height="450" align="center">

Ces données révèlent une variation dans le nombre de chansons explicites parmi les artistes étudiés. Le fait que certains artistes aient un nombre élevé de chansons explicites peut refléter leur style musical, leurs paroles provocantes ou leurs choix artistiques audacieux.
La présence de paroles explicites peut être un critère important pour certains auditeurs lorsqu'ils choisissent leur musique. Cette information fournit un aperçu des artistes qui sont plus enclins à produire des chansons avec un contenu explicite.
Cependant, il convient de souligner que la présence de chansons explicites ne définit pas nécessairement la qualité artistique ou la popularité d'un artiste. Les préférences et les sensibilités des auditeurs peuvent varier considérablement en ce qui concerne le contenu des chansons.

### 7) Quelle est la popularité moyenne des chansons explicites versus non-explicites ?
<img src="visualisation-img/7.png" height="300" align="center">

- L'analyse du graphe révèle que près de 52,9 % des chansons dans le jeu de données sont explicites, tandis que 47,1 % sont non-explicites. Cela souligne une présence significative de chansons explicites dans la collection étudiée.

- La popularité des chansons explicites peut être attribuée à leur capacité à aborder des thèmes et des sujets plus audacieux, ce qui peut susciter l'intérêt et l'engagement des auditeurs.

- Ces résultats mettent en évidence une prévalence des chansons explicites dans l'industrie musicale actuelle.
Cela souligne une tendance dans l'industrie musicale où les chansons explicites occupent une part importante du marché. Cela peut être dû à divers facteurs tels que l'évolution des préférences des auditeurs, l'expression artistique plus franche ou la volonté des artistes de créer des œuvres plus authentiques et sans censure.

### 8) Quelle est la répartition de la liveness par genre pour les 5 genres les plus populaires ?
<img src="visualisation-img/8.png" height="400" align="center">

L'analyse de ce graphique révèle la répartition de la "liveness" par genre pour les cinq genres les plus populaires 
La liveness se réfère à la probabilité qu'une chanson ait été enregistrée en direct, capturant ainsi l'énergie et l'authenticité d'une performance sur scène.
L'analyse met en évidence des différences dans l'utilisation d'enregistrements en direct au sein de chaque genre. Les genres tels que "Metal" et "K-Pop" se démarquent avec une liveness plus élevée c'est à dire qu'ils sont susceptible de proposer des enregistrements en direct fréquents, reflétant ainsi l'aspect dynamique et puissant de ce style musical, tandis que d'autres genres comme "Latino", "Pop-Film" et "Chill" présentent également une certaine proportion de performances en direct, bien que légèrement inférieure. Ces résultats nous permettent d'appréhender les caractéristiques et les atmosphères spécifiques associées à chaque genre musical en fonction de leur liveness respective.

### 9) Quels sont les 5 artistes avec le plus d'albums ?
<img src="visualisation-img/9.png" height="350" align="center">

L'analyse du graphique met en évidence les cinq artistes qui ont le plus grand nombre d'albums dans le jeu de données Spotify.
Ces résultats nous donnent un aperçu des artistes les plus prolifiques en termes de nombre d'albums dans le jeu de données Spotify. Il est intéressant de constater la diversité des genres musicaux représentés par ces artistes, ce qui suggère qu'ils ont une production musicale variée et étendue. Ces informations pourraient être utiles pour les utilisateurs de Spotify qui souhaitent explorer davantage la discographie de ces artistes ou qui recherchent une large sélection d'albums à découvrir.

### 10) Quels sont les 5 genres avec le plus grand nombre de chansons instrumentales ?
<img src="visualisation-img/10.png" height="350" align="center">

L'analyse révèle que le genre "Study" se distingue avec environ 900 chansons instrumentales. Cela suggère que ce genre est particulièrement propice à la création de morceaux sans paroles, peut-être pour accompagner les moments d'étude, de concentration ou de relaxation. Il est suivi de près par "Sleep" et "Minimal-Techno" avec près de 800 chansons chacun. Les genres "New Age" et "IDM" comptent également environ 850 chansons instrumentales. Ces résultats indiquent une prédominance de la musique instrumentale dans ces genres, suggérant ainsi qu'ils offrent un large choix de morceaux sans paroles. Cela démontre également la diversité des genres musicaux où les chansons instrumentales sont présentes, allant de la musique d'ambiance relaxante à des sonorités électroniques plus expérimentales.



