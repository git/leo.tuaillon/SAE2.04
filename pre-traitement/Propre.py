import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import display
from sklearn.linear_model import LinearRegression
from sklearn import datasets
import seaborn as sns
import statsmodels.api as sm
import statsmodels.formula.api as smf


# Charger les données depuis un fichier CSV
data = pd.read_csv('visualisation/dataset.csv')
df = pd.DataFrame(data)

# Compter le nombre de genres musicaux dans chaque catégorie
a = df['track_genre'].value_counts()
print(a)

# Supprimer les doublons
df = df.drop_duplicates(subset=['track_id'])
print(df.shape)


# Convertir chaque catégorie de genre musical en un entier distinct basé sur la valeur de la catégorie
df['track_genre'] = df['track_genre'].astype('category')

# Obtenir les genres uniques et leur nombre
genre = df["track_genre"].unique()
print(genre)
print("\nNombre de genre :", len(genre))


df2 = df.drop(["Unnamed: 0", "key", "mode", "time_signature"], axis=1)
display(df2)

with open('visualisation/dataPropre.csv', 'w') as csv_file:
    df2.to_csv(path_or_buf=csv_file)

df2["duration_s"] = df2["duration_ms"] / 1000
df2 = df2.drop(["duration_ms"], axis=1)
display(df2)



# Sélectionner les colonnes à utiliser pour la régression linéaire
X = df2[['duration_s']]
y = df2['popularity']

# Créer un modèle de régression linéaire et l'entraîner sur les données
model = LinearRegression()
model.fit(X, y)

# Faire une prédiction pour toutes les observations
y_pred = model.predict(X)

# Créer un graphique montrant la relation entre la durée de la chanson et sa popularité
fig, ax = plt.subplots(figsize=(10, 6))
ax.scatter(X, y, alpha=0.5, color='b')
ax.plot(X, y_pred, color='r')
ax.set_xlabel('Durée de la chanson (ms)')
ax.set_ylabel('Popularité')
ax.set_title('Durée de la chanson en fonction de la popularité')

# Faire des prédictions de popularité pour des durées de chanson spécifiques
prediction = model.predict([[60000]])
print(f'La prédiction de la popularité est de {prediction} pour une durée de 1 minute')

prediction = model.predict([[180000]])
print(f'La prédiction de la popularité est de {prediction} pour une durée de 3 minutes')

prediction = model.predict([[300000]])
print(f'La prédiction de la popularité est de {prediction} pour une durée de 5 minutes')
plt.show()
'''
Ce code permet de faire des prédictions de popularité pour des durées de chansons spécifiques 
(3 minutes et 5 minutes), puis d'afficher la matrice de corrélation et enfin un heatmap de la matrice 
de corrélation pour visualiser les relations entre les différentes variables.

On remarque que les données ne sont pas intéressante et donc peu fiables car la corélation entre le temps de 
la musique et la popularité est faible.

Nous allons donc cherchez des données plus intéréssante avec une corrélation plus forte entre les deux. 
Pour celà l'idée serai de faire une matrice de corélation : 

Une matrice de corélation est un tableau qui permet de mesurer la force et la direction des relations 
linéaires entre les différentes variables d'un ensemble de données. Chaque cellule de la matrice indique 
le coefficient de corrélation entre deux variables. Le coefficient de corrélation varie entre -1 et 1 :

Si le coefficient est proche de 1, cela indique une relation linéaire positive entre les deux variables. 
Lorsque l'une des variables augmente, l'autre variable a également tendance à augmenter.
Si le coefficient est proche de -1, cela indique une relation linéaire négative entre les deux variables. 
Lorsque l'une des variables augmente, l'autre variable a tendance à diminuer.
Si le coefficient est proche de 0, cela indique qu'il n'y a pas de relation linéaire significative entre 
les deux variables.
'''

# Création de la matrice de corrélation
corr_matrix = df2.corr()

# Afficher un heatmap de la matrice de corrélation
plt.figure(figsize=(12, 8))
plt.title('Matrice de corrélation', fontsize=16)
sns.heatmap(corr_matrix, annot=True, cmap='coolwarm')
plt.show()
'''
La matrice de corrélation 
Ici grace à la matrice de correlation on remarque que dans notre dataframe, nous pouvons rapidement repérer
les données avec une corelation intéréssente à analyser, nous nous interessons donc aux données ayant une corélation proche de 1 ou de -1.
J'ai choisis de m'intéressé à la corélation entre la loudness et l'instrumentalness qui à une corélation de -0.43, c'est une valeur négative,
ce qui signifie donc que lorsque l'une des variables augmente, l'autre variable a tendance à diminuer.
'''


# Sélectionner les colonnes à utiliser pour la régression linéaire
X = df2[['instrumentalness']]
y = df2['loudness']

# Créer un modèle de régression linéaire et l'entraîner sur les données
model = LinearRegression()
model.fit(X, y)

# Faire une prédiction pour toutes les observations
y_pred = model.predict(X)

# Créer un graphique montrant la relation entre la durée de la chanson et sa popularité
fig, ax = plt.subplots(figsize=(10, 6))
ax.scatter(X, y, alpha=0.5, color='b')
ax.plot(X, y_pred, color='r')
ax.set_xlabel('Instrumentalness')
ax.set_ylabel('Loudness')
ax.set_title('Loudness en fonction de l\'instrumentalness')

# Faire des prédictions de popularité pour des durées de chanson spécifiques
prediction = model.predict([[0]])
print(f'La prédiction de loudness est de {prediction} pour une l\'instrumentalness de 0')

prediction = model.predict([[50]])
print(f'La prédiction de loudness est de {prediction} pour une l\'instrumentalness de 50')

prediction = model.predict([[100]])
print(f'La prédiction de loudness est de {prediction} pour une l\'instrumentalness de 100')
plt.show()


# Ajouter une colonne de la variable indépendante au carré (instrumentalness^2)
df['instrumentalness_squared'] = df['instrumentalness']**2

# Utiliser smf.ols pour ajuster le modèle polynomiale de degré 2
model_poly = smf.ols(formula='loudness ~ instrumentalness + instrumentalness_squared', data=df).fit()

# Affichage d'un résumé des résultats du modèle
print(model_poly.summary())

# Prédiction pour toutes les observations
y_pred_poly = model_poly.predict(df[['instrumentalness', 'instrumentalness_squared']])



#=============YVAN


#------------------Regression linéaire n°1---------------------

# Sélectionner les colonnes à utiliser pour la régression linéaire
X = data[['valence']]
y = data['danceability']


# Créer un modèle de régression linéaire et entraîner sur les données
model = LinearRegression()
model.fit(X, y)

# Faire une prédiction pour toutes les observations
y_pred = model.predict(X)


# Créer un graphique
fig, ax = plt.subplots(figsize=(10, 6))
ax.scatter(X, y, alpha=0.5, color='b')
ax.plot(X, y_pred, color='r')
ax.set_xlabel('Valence')
ax.set_ylabel('Danceability')
ax.set_title('Danceability en fonction de la Valence')


prediction_1 = model.predict([[0.1]])
prediction_2 = model.predict([[0.5]])
prediction_3 = model.predict([[0.8]])

print(f'La prédiction de la danceability est de {prediction_1} pour une Valence de 0.1')
print(f'La prédiction de la danceability est de {prediction_2} pour une Valence de 0.5')
print(f'La prédiction de la danceability est de {prediction_3} pour une Valence de 0.8')



#------------------Regression linéaire n°2---------------------

X = data[['energy']]
y = data['loudness']


# Créer un modèle de régression linéaire et entraîner sur les données
model = LinearRegression()
model.fit(X, y)

# Faire une prédiction pour toutes les observations
y_pred = model.predict(X)


# Créer un graphique
fig, ax = plt.subplots(figsize=(10, 6))
ax.scatter(X, y, alpha=0.5, color='b')
ax.plot(X, y_pred, color='r')
ax.set_xlabel('energy')
ax.set_ylabel('loudness')
ax.set_title('loudness en fonction de l\'energy')


prediction_1 = model.predict([[0.1]])
prediction_2 = model.predict([[0.5]])
prediction_3 = model.predict([[0.8]])

print(f'La prédiction de la loudness est de {prediction_1} pour une energy de 0.1')
print(f'La prédiction de la loudness est de {prediction_2} pour une energy de 0.5')
print(f'La prédiction de la loudness est de {prediction_3} pour une energy de 0.8')

plt.show()



#===============Mathéo===============#

#Regressions linéaires simples pour prédire la speechiness en fonction de la duration_min
X = df2["duration_s"].values.reshape(-1, 1)
y = df2["speechiness"].values.reshape(-1, 1)
reg = LinearRegression()
reg.fit(X, y)
print("R² = ", reg.score(X, y))
print("a = ", reg.coef_)
print("b = ", reg.intercept_)


# Créer un modèle de régression linéaire
df2["duration_s"] = df["duration_ms"] / 1000

# Sélectionner les variables indépendantes (durée de la chanson) et dépendante (speechiness)
X = df2[["duration_s"]]
y = df2["speechiness"]

# Ajouter une constante pour estimer l'intercept
X = sm.add_constant(X)

# Créer un modèle de régression linéaire
model = sm.OLS(y, X)

# Entraîner le modèle sur les données
results = model.fit()

# Afficher les résultats de la régression
print(results.summary())

# Faire des prédictions pour des durées spécifiques
prediction_1min = results.predict([1, 1])
prediction_3min = results.predict([1, 3])
prediction_5min = results.predict([1, 5])

# Afficher les prédictions
print(f'La prédiction de la speechiness est de {prediction_1min} pour une durée de 1 minute')
print(f'La prédiction de la speechiness est de {prediction_3min} pour une durée de 3 minutes')
print(f'La prédiction de la speechiness est de {prediction_5min} pour une durée de 5 minutes')

# Tracer le nuage de points et la droite de régression linéaire
fig, ax = plt.subplots(figsize=(10, 6))
ax.scatter(X.iloc[:,1], y, alpha=0.5, color='b')
y_pred = results.predict(X)
ax.plot(X.iloc[:,1], y_pred, color='r')
ax.set_xlabel('Durée de la chanson (min)')
ax.set_ylabel('Speechiness')
ax.set_title('Durée de la chanson en fonction de la speechiness')

plt.show()

'''
Ici on remarque que la régression linéaire n'est pas adaptée à ce type de données car la speechiness est très faible pour les chansons de plus de 5 minutes.
On va donc utiliser une matrice de corrélation pour voir si on peut trouver une autre variable qui pourrait être plus adaptée.

La matrice de corrélation sert à voir si deux variables sont corrélées entre elles.
Si c'est proche de 1, alors elles sont fortement corrélées.
Si c'est proche de -1, alors elles sont fortement corrélées mais inversées.
'''


# Calculer la matrice de corrélation
corr_matrix = df2.corr()

# Visualiser la matrice de corrélation avec Seaborn
plt.figure(figsize=(12, 8))
plt.title('Matrice de corrélation')
sns.heatmap(corr_matrix, annot=True)
plt.show()

#On remarque que une variable qui a beaucoup de corrélation avec la loudness est l'accousticness.
#Car la corrélation est de -0.59.
#On va donc utiliser cette variable pour prédire la loudness.

X = df2[["acousticness"]]
y = df2["loudness"]

# Ajouter une constante pour estimer l'intercept
X = sm.add_constant(X)

# Créer un modèle de régression linéaire
model = sm.OLS(y, X)

# Entraîner le modèle sur les données
results = model.fit()

# Afficher les résultats de la régression
print(results.summary())

# Faire des prédictions pour des accousticness spécifiques
prediction_0 = results.predict([1, 0])
prediction_1 = results.predict([1, 1])

# Afficher les prédictions
print(f'La prédiction de la loudness est de {prediction_0} pour une accousticness de 0')
print(f'La prédiction de la loudness est de {prediction_1} pour une accousticness de 1')

# Tracer le nuage de points et la droite de régression linéaire
fig, ax = plt.subplots(figsize=(10, 6))
ax.scatter(X.iloc[:,1], y, alpha=0.5, color='b')
y_pred = results.predict(X)
ax.plot(X.iloc[:,1], y_pred, color='r')
ax.set_xlabel('Accousticness')
ax.set_ylabel('Loudness')
ax.set_title('Accousticness en fonction de la loudness')

plt.show()

'''
On remarque que la régression linéaire est beaucoup plus adaptée à ce type de données.
On peut donc prédire la loudness en fonction de l'accousticness.
'''

#On va maintenant calculer le coefficient de détermination R² pour voir si la régression linéaire est adaptée à ce type de données.
#On va donc utiliser la méthode du score.

# Calculer le coefficient de détermination R²
r2 = results.rsquared
print(f'Le coefficient de détermination R² est de {r2}')


#On remarque que le coefficient de détermination R² est de 0.35.
#Ce qui veut dire que la régression linéaire est adaptée à ce type de données.
